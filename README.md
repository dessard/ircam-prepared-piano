> - Retail Price: 299 € VAT included
> - [Premium member](https://www.ircam.fr/innovations/abonnements-du-forum/) price: 249 € VAT included
> - Only available by downloading
> - Collaboration: [UVI](https://www.uvi.net/en/)
> - [buy](https://www.ircam.fr/innovations/abonnements-et-logiciels/)

## The Ultimate Avant-Garde Piano ##

The ultimate virtual prepared piano experience, brought to you by [UVI](https://www.uvi.net/en/) and the renowned [IRCAM](https://www.ircam.fr/) acoustic research center in Paris. We brought together the world’s finest engineers and equipment to comprehensively multi-sample 45 preparations at IRCAM Labs on a Yamaha C7 Grand Piano. 

The result is the most flexible and exquisite sounding prepared piano instrument on the market. Freely customize your instrument with up to two preparations per note, mix multiple mic positions and quickly explore new sonic landscapes with the randomizer. Try the included preparations or make your own in minutes! Truly the most advanced, flexible, and comprehensive prepared piano instrument ever made, IRCAM Prepared Piano leverages the power of the UVI Engine in ways never before seen.

## Normal playing techniques ##

Sustain and None A normal piano sound. The strings are struck by hammers, triggered by the notes played on the keyboard. Use of the una corda and forte pedals is possible. There is no object in or on the strings modifying the sound.

The none preparation setting does not refer to there being no object or preparation modifying the sound of the string, but rather to the lack of sound produced by playing this key –  a form of preparation that is impossible without breaking the hammer. This setting can be very useful in preventing the sounding of misplayed notes, or for playing Gyorgy Ligeti’s Etude: Touches bloquées (by cheating, a little…)

## Expanded playing techniques ##

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Piano%20Prepared/piano-prepare_2.jpg)

*Striking the Strings*

-	**Mallets:** The strings are struck with a vibraphone mallet. The resulting sound is similar to a normal key strike, but is slightly softer due differences between the mallet and the normal hammer action.
-	**Wooden Stick:** The strings are struck directly using wooden sticks. The attack is harder and shorter. The timbre is similar to that of a dulcimer, and can be activated by pressure on the forte pedal.
-	**Stick Rebound:** This setting features the same technique and resulting timbre as the Wooden Stick setting, but allows the stick to bounce on the string after the initial strike.
-	**Muted Stick Rebound:** This setting features the same technique as Stick Rebound, but uses the stick on muted strings (see mutes).

*Plucking or Strumming the Strings with the Fingers*

-	**Pizz.:** The strings are plucked with a plectrum, one string only per note. This results in a sound resembling that of a guitar. It is a widely used effect, for example by Georg Crumb in Makrokosmos, or by Gerard Pesson in Rescousse.
-	**Scratch:** A fast scraping action by the nail along the string. When used on the bass strings, this causes a burst of harmonics. One can hear this technique, in combination with numerous other piano string effects, in Clepsydre by Horacio Radulescu (for 16 sound icons, or pianos installed vertically and played exclusively on the strings).

*Sustained Sounds*

-	**Bow:** Rosin horsehair (a single strand of a violin bow) causing the two or three strings that comprise a single pitch to vibrate. The resulting sound is continuous. Changes in the direction of the bow are always audible and give transient color to the timbre. This effect is sometimes made with fishing line rosin.
-	**Ebow:** A small device normally used on electric guitars, causing the strings of the piano to vibrate through a magnetic field. This produces a continuous sound with a masked attack, resulting in a delicate and subtle timbre.

*Objects Inserted Between the Strings*

Objects may be inserted between the strings. From the treble keys down to medium bass (F1), there are three strings per note. In this range it is possible to insert one object between strings 1 & 2, and another in the strings 2 & 3.

Similarly, in IRCAM Prepared Piano, you can continue to add several more objects or playing techniques from those available when there are only two strings (F # 0 - E1) or one (as for F0). When the una corda pedal is pressed, only the preparations on strings 2 and 3 are activated, meaning that the pedal is functioning in line with organ techniques.

-	**Rubber:** Large pencil erasers are used for this preparation, producing a sound similar to that of a deep bass gong. In the first movement of John Cage’s Amores, three notes are muted with rubber giving a «touches bloquées» effect in contrast to other resonant preparations.

-	**Half Spring-type Clothespin:** The half-spring pegs found here represent preparations made from wood. The hardness of the wood used in the pegs results in the production of gong-like sonorities. To simulate preparations using softer wood (which give a duller, dampened sound), half spring pegs can be combined with muted or rubber preparations. Wooden preparations sometimes touch the soundboard, producing a short, hard attack when the string is struck. It is possible to achieve this effect by ticking «add soundboard.»

-	**Screw/Bolt (tapered screw/bolt screw):** The screw, or bolt, is the most common preparation. There are screws of varying lengths and thicknesses, as well as screws with large heads, small heads, tapered heads or un-tapered heads. All of these factors affect the sonic characteristics when applied as a preparation to a piano string.

The preparation chosen uses a wide range of materials to create a variety of timbres from one note to another. The sounds are generally rich and resonant; they remain within the gong family, featuring a small vibration when the note is played forte.

-	**Screw/Bolt + Loose Nut (buzz):** The same preparation described above, but with a loose nut around the screw which bounces when the note is played, creating a «buzz». An example of this preparation can be found in the first movement of John Cage’s Amores, where two notes prepared using screws must also be topped with nuts. This work also serves as a concise overview of the preparations Cage employed using mixed materials.

-	**Coins:** A coin is inserted between the strings (above 1 and 3 and below 2), greatly enriching the sound. The result resembles a rich yet soft gong, sometimes featuring a very slight vibration. The sound is generally more rounded than when the strings are prepared with screws. The low notes of John Cage’s A Room are a fine example of the use of coins, combined with screws used on other notes.

*Objects Placed on the Strings*

For the following preparations, the string is normally struck by the hammer. Objects can be placed on the strings, altering the manner in which they sound. The ones placed on the strings in this manner would normally affect several notes in succession on a real piano, which is not mandatory in IRCAM Prepared Piano.

-	**Paper:** Sheet of writing paper, rolled up and placed on the strings. Its design and the resulting tone echo the bassoon of some pianofortes.

-	**Aluminum:** Aluminum foil placed flat on the strings. This gives a fairly long and characteristically metallic vibration. It is similar to preparations that consist of placing metal chains on the strings, as in the first work, Primeval Sounds, in Georg Crumb’s Makrokosmos.

-	**iPhone:** An iPhone resting flat on the strings. This gives a slightly metallic damping effect, as well as an occasional bouncing of the iPhone when the strings are struck. This is due to the iPhone not being tethered to the piano strings.

-	**Mutes:** The string is dampened with a finger, close to the bridge. This category of dampened sounds includes preparations inserted between the strings, using fabric, felt (Felt Temperament Strip), or window seal, as well as variations in angle that influence the overall time effect of the damping. Studies by Michael Levinas exploit this technique.

*Effects*

-	**Glass Slide:** A stemless glass is held on the strings, maintaining constant pressure, and slid lengthwise along them. The glissando is ascending or descending, depending on whether the slide is moving towards or away from the dampers.

-	**Bar Hits:** A percussive impact on the bars of the piano’s cast-iron plate, above the soundboard. Three playing techniques are used: the hands, soft drumsticks and hard drumsticks. These samples are played on the lowest bass notes beyond A-1. In Caravan, from Jacky Terrasson’s album Mirror, there is an improvisation using the hands inside the piano frame.

-	**Extra Length:** A plectrum slide along the part of the strings located between the bridge and the tuning pins. Since this is an unusual two-handed polyphonic technique, two potentiometers are used to produce this effect.

![IRCAM Prepared Piano](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Piano%20Prepared/piano-prepare.jpg)

## Overview ##

-	Size: 4.36 GB (FLAC lossless encoding, was 19 GB in WAV)
-	Content: 1 2,572 Samples
-	Sample Resolution: 4 4.1 kHz. Recorded at 96 kHz
-	License: 3 activations per license on any combination of machines or iLok dongles

## System requirements ##

-	Supported Operating Systems: Mac OS X 10.8 to macOS 10.14 Mojave (64-bit) or Windows 8 to Windows 10 (64-bit)
-	5GB of disk space
-	Hard Drive: 7,200 rpm recommended or Solid State Drive (SSD)
-	4GB RAM (8 GB+ highly recommended for large UVI Soundbanks)
-	Runs in [UVI Workstation](http://www.uvi.net/uvi-workstation.html) version 2.6.11+, and [Falcon](http://www.uvi.net/falcon.html) version 1.3.0+
-	[iLok](https://www.ilok.com/) account (free, dongle not required)
-	Internet connection for the license activation
-	
## Compatibility ##

-	Supported Formats: Audio Unit, AAX, VST, Standalone
-	Tested and Certified in: Ableton Live 8+, Pro Tools 11+, Cubase 7+, Digital Performer 8+, Logic 9+, Nuendo 6+, Studio One 2+, Garage Band 6+, Maschine 1+, Tracktion 4+, Vienna Ensemble Pro 5+, Reaper 4+, Sonar X3+, MainStage 3, MuLab 5.5+, FL Studio, Bitwig 1+, Reason 9.5+

> - [Soundbank installation Guide](https://s3.amazonaws.com/uvi/UVI/installing_uvi_soundbanks_en.pdf)

